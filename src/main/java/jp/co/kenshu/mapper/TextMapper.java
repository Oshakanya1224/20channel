package jp.co.kenshu.mapper;

import java.sql.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.Text;

public interface TextMapper {

	//一件取得
	Text getText(int id);

	//全件取得
	List<Text> getTextAll();

	//データベースに投稿と更新日時追加
	int insertText(@Param("message")String message, @Param("createdDate")Date createdDate, @Param("updatedDate")Date updatedDate);

	//投稿削除
	//is_stoppedを使って表面上投稿削除(データベースから削除しない)
	//削除ボタン押されたときに1をデータベースに挿入
	int messageDelete(@Param("id")int id, @Param("isStopped")int isStopped);
}
