package jp.co.kenshu.service;

import java.sql.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.text.CommentTextDto;
import jp.co.kenshu.dto.text.TextDto;
import jp.co.kenshu.entity.CommentText;
import jp.co.kenshu.entity.Text;
import jp.co.kenshu.mapper.CommentTextMapper;
import jp.co.kenshu.mapper.TextMapper;

@Service
public class TextService {
	@Autowired
    private TextMapper textMapper;

	//一件取得
    public TextDto getText(Integer id) {
        TextDto dto = new TextDto();
        Text entity = textMapper.getText(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    //全件取得
    public List<TextDto> getTextAll() {
        List<Text> textList = textMapper.getTextAll();
        List<TextDto> resultList = convertToDto(textList);
        return resultList;
    }

    private List<TextDto> convertToDto(List<Text> textList) {//ここのtextList
        List<TextDto> resultList = new LinkedList<>();
        for (Text entity : textList) {//とここのtextListが対応
            TextDto dto = new TextDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    //データベースに投稿追加
    public int insertText(String message, Date createdDate, Date updatedDate) {
        int count = textMapper.insertText(message, createdDate, updatedDate);
        return count;
    }

    //投稿削除
  	//is_stoppedを使って表面上投稿削除(データベースから削除しない)
  	//削除ボタン押されたときに1をデータベースに挿入
    public int messageDelete(int id, int isStopped) {
    	int count3 = textMapper.messageDelete(id, isStopped);
    	return count3;
    }

    //----------------------------------------------------------------------

    //データベースからコメント全件取得
    @Autowired
    private CommentTextMapper commentTextMapper;

	public List<CommentTextDto> getCommentTextAll() {
		 List<CommentText> commentTextList = commentTextMapper.getCommentTextAll();
	        List<CommentTextDto> resultCommentList = convertToCommentDto(commentTextList);
	        return resultCommentList;
	    }

	private List<CommentTextDto> convertToCommentDto(List<CommentText> commentTextList) {//ここのtextList
        List<CommentTextDto> resultCommentList = new LinkedList<>();
        for (CommentText entity : commentTextList) {//とここのtextListが対応
            CommentTextDto dto = new CommentTextDto();
            BeanUtils.copyProperties(entity, dto);
            resultCommentList.add(dto);
        }
        return resultCommentList;
    }

	 //データベースにコメント追加
    public int insertCommentText(String comment, int messageId , Date createdDate, Date updatedDate) {
        int count1 = commentTextMapper.insertCommentText(comment, messageId, createdDate, updatedDate);
        return count1;
    }

    //コメントをアップデートした日時を更新
    public int updatedComment(int messageId) {
        int count2 = commentTextMapper.updatedComment(messageId);
        return count2;
    }

    //コメント削除
  	//is_stoppedを使って表面上コメント削除(データベースから削除しない)
  	//削除ボタン押されたときに1をデータベースに挿入
    public int commentDelete(int id, int isStopped) {
    	int count4 = commentTextMapper.commentDelete(id, isStopped);
    	return count4;
    }
}

