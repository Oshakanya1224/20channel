<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Welcome</title>
    </head>
    <body>
     	<!-- データベースの投稿全件表示 -->
    	<!-- このsentence,texts,textはControllerと対応 -->

        <h1>${sentence}</h1>

        <!-- バリデーション -->
		<h1>
	       <c:out value="${title2}"></c:out>
	    </h1>

	    <p>
	       <c:out value="${message2}"></c:out>
	    </p>

	    <a href="${pageContext.request.contextPath}/20ch/insert/input/">新規投稿</a>

		<!-- データベースの投稿全件表示 -->
        <!-- var=textはドルカッコ内のtextと一致 -->
        <c:forEach items="${texts}" var="text">
        	<c:if test="${text.isStopped == 0}">
	        	<!-- getter.setterのmessageと対応 -->
	            <p><c:out value="${text.message}"></c:out></p>
	        </c:if>

			<!-- 投稿削除フォーム -->
			<form:form modelAttribute="messageDeleteForm" >
				<form:hidden path="id" value="${text.id}" />
				<form:hidden path="isStopped" value="1"/>
				<!-- ControllerのParamsとname=の部分紐づけ -->
	   			<input type="submit" value="削除" name="messageDelete" onClick="return Check();"/>
	   		</form:form>
	   		<c:if test = "${text.isStopped == 1}">
	   			<p><c:out value="この投稿は管理者によって削除されました"></c:out></p>
	   		</c:if>

	        <!-- データベースのコメント全件表示 -->
	        <!-- var="comment"は変数名みたいなもんで、
	        	 このjspでは"comment"を使っていくよって言っている。
	        	 "comment"の中にControllerと繋がっているcommentsを入れている。
	        	 なので、下記の29.31行目のcomment.のcommentはcommentの中の、
	        	 messageIdとcommentを取ってきている。 -->

	        <c:forEach items="${comments}" var="comment">
	        	<!-- ここでmessagesテーブルのidとcommentsテーブルのmessage_id -->
				<c:if test="${comment.messageId == text.id}">
					<c:if test="${comment.isStopped == 0}">
		        		<!-- 右commentがgetter.setterのと対応 -->
		           	 	<p><c:out value="${comment.comment}"></c:out></p>
		           	 </c:if>

	           	 	<!-- コメント削除フォーム -->
	           	 	<form:form modelAttribute="commentDeleteForm" >
						<form:hidden path="id" value="${comment.id}" />
						<form:hidden path="isStopped" value="1"/>
						<!-- ControllerのParamsとname=の部分紐づけ -->
			   			<input type="submit" value="コメント削除" name="commentDelete" onClick="return Check();"/>
			   		</form:form>
			   		<c:if test = "${comment.isStopped == 1}">
			   			<p><c:out value="このコメントは管理者によって削除されました"></c:out></p>
			   		</c:if>
			    </c:if>
	        </c:forEach>

	        <!-- コメント入力フォーム -->
	        <!-- コメントをデータベースに追加 -->
		    <form:form modelAttribute="commentTextForm">

		    	<!-- バリデーション用form:errorsタグの追加 -->
			    <!-- path=以降はFormと対応 -->
			    <div><form:errors path="comment"/></div>

		    	<!-- setter,getterのcomment -->
		    	<!-- springはpathじゃないと動かん -->
		    	<!-- pathはentityに対応 -->
		        <form:textarea path="comment" cols="20" rows="5"/>
		        <input type="submit" value="コメントしちゃうヨ">
		        <!-- コメントを送るのと同時に、一件取得の時のtextっていう名前の箱に入ってるidを送るのでtext.id
		        	 そのtext.idはmessagesテーブルのid -->
			    <form:hidden path="messageId" value="${text.id}" /><br/>
		    </form:form>
	    </c:forEach>

		<!-- 投稿＆コメント消去実行コード -->
		<script type="text/javascript">
			function Check(){
		    var word;
		    word = prompt("合言葉は? ", "");
		    	if(word == "かねき"){
		    		return true;
		    	} else {
			    	window.alert("合言葉が違います");
					return false;
		    	}
			}
	  	</script>
    </body>
</html>